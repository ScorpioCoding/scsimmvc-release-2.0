<?php
/**
*  Base Controller
*/
class Controller extends Functions
{

	protected $_scStyles = array('scStyles' => array() );

	public function __construct()
    {
		parent::__construct();
		$this->view = new View();
	}

	/*
	* Loads the Model
	* @params string $model
	*/
	protected function model($model)
	{
		$file = PATH_MODEL . $model .'.php';
		if ( file_exists( $file ) )
		{ 
			require($file);
			return new $model();
		}	
		else
			throw new Exception("Controller.php : model : File doesnt exist : $file");	
	}
	
	protected function getScVariables()
	{
		$file = PATH_LIBS .'scVariables.php';
		if ( file_exists( $file ) )
		{ 
			require($file);
			return $scVariables;
		}
		else
			throw new Exception("Controller.php : getScVariables : File doesnt exist : $file");
	}

	/*
	* Loads the Css call- files you request
	* @params string $name without
	*/
	protected function setStyles($name)
	{
		$file = PATH_CSS . $name.'.css';
		if ( file_exists( $file ) )
		{ 
			$a  = " <link rel='stylesheet' href=' ". PATH_ABS ."css/". $name .".css  ' />    \n\t\t\r"    ;			
			array_push( $this->_scStyles['scStyles'], $a);
		}
		else
			throw new Exception("Controller.php : getStyles : File doesnt exist : $file");
	}

	protected function getStyles()
	{
		return $this->_scStyles;
	}

	protected function translation($translation)
	{
		//a method to acces / bind the translation to a view/ Model variables
		//eg.    in the template header the title
		// trans.en.php  en  - title = english
		// trans.nl.php  nl  - title = nederlands

		//these translation files can be outsourced

		//these translations are for our core elements and not dynamic centent...
		//like buttons, links, headers, fixed content,

		// the translation array is then sent to the view where the arry is extracted into seperate variables
		// which can be found in the tempplates.

		// $file = PATH_MODEL . $model .'.php';
		// if ( file_exist( $file ) )
		// { 
		// 	require($file);
		// 	return new $model();
		// }		
	}

}//END CLASS
?>
