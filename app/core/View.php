<?php

/**
*  Base View
*/
class View extends Functions
{
	public function __construct()
    {
		parent::__construct();
	}

	/*
	* render the view
	* @params int 		$renderOption  (f0, f1, b0, b1) 0: noInclude, f: frontend, b: backend,
	* @params string 	$name	
	* @params array 	$data
	*/
	public function render($renderOption = 1, $name, $data = array() )
	{
		$paths = array (
			'f0' => 
				array ( 1 => PATH_F_VIEW . $name . '.phtml'),
			'f1' =>
				array ( 1 => PATH_F_VIEW_TMP . 'header.phtml',
						2 => PATH_F_VIEW . $name . '.phtml',
						3 => PATH_F_VIEW_TMP . 'footer.phtml'),
			'b0' => 
				array ( 1 =>  PATH_B_VIEW . $name . '.phtml'),
			'b1' =>
				array ( 1 => PATH_B_VIEW_TMP . 'header.phtml',
						2 => PATH_B_VIEW . $name . '.phtml' ,
						3 => PATH_B_VIEW_TMP . 'footer.phtml'
			));

		
		self::renderPage($renderOption, $paths, $data );			
	}

} //END CLASS
?>

