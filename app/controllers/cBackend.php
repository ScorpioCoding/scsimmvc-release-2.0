<?php

/**
 *  cBackend
 */
class Backend extends Controller
{
	
	public function __construct()
	{
		parent::__construct();
	}

	public function dashboard()
	{
        $this->view->render('b1', 'dashboard');
	}

	public function login()
	{
			$this->view->render('b0', 'login');
	}

	public function logout()
	{
			$this->view->render('b1', 'logout');
	}

	public function project()
	{
			$this->view->render('b1', 'project');
	}

	public function whatevercomesnext()
	{
			$this->view->render('b0', 'whatevercomesnext');
	}
}